﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTimeOffset? BeginDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public string PartnerName { get; set; }

        public Guid PartnerManagerId { get; set; }
        public Employee PartnerManager { get; set; }

        public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }

        public List<Customer> Customers { get; set; }
        public List<CustomerPromoCode> CustomerPromoCodes { get; set; }
    }
}