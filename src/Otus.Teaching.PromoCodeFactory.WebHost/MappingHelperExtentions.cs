﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    internal static class MappingHelperExtentions
    {
        internal static CustomerShortResponse ToCustomerShortResponse(this Customer customer)
            => new CustomerShortResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

        internal static CustomerResponse ToCustomerResponse(this Customer customer)
            => new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes?.Select(pc => pc.ToPromoCodeShortResponse()).ToList(),
                Preferences = customer.Preferences?.Select(p => p.ToPreferenceResponse()).ToList()
            };

        internal static PromoCodeShortResponse ToPromoCodeShortResponse(this PromoCode promo)
            => new PromoCodeShortResponse
            {
                Id = promo.Id,
                Code = promo.Code,
                ServiceInfo = promo.ServiceInfo,
                BeginDate = promo.BeginDate.ToString(),
                EndDate = promo.EndDate.ToString(),
                PartnerName = promo.PartnerName
            };

        internal static Customer ToCustomer(this CreateOrEditCustomerRequest request) 
            => request.ToCustomerWith(null);

        internal static Customer ToCustomerWith(
            this CreateOrEditCustomerRequest request,
            IEnumerable<Preference> preferences)
            => new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences?.ToList() ?? new List<Preference>()
            };

        internal static PreferenceResponse ToPreferenceResponse(this Preference preference)
            => new PreferenceResponse
            {
                Id = preference.Id,
                Name = preference.Name
            };

        internal static PromoCode ToPromoCode(this GivePromoCodeRequest request)
            => new PromoCode
            {
                Code = request.Code,
                ServiceInfo = request.ServiceInfo,
                PartnerManagerId = request.PartnerId,
                PreferenceId = request.PreferenceId,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate
            };
    }
}
