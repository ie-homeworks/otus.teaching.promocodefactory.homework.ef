﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PreferenceId { get; set; }

        public DateTimeOffset BeginDate { get; set; }

        public DateTimeOffset EndDate { get; set; }
    }
}