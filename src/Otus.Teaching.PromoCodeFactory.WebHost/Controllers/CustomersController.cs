﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet]
        [ProducesResponseType(typeof(CustomerShortResponse[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(c => c.ToCustomerShortResponse());

            return Ok(response);
        }

        /// <summary>
        /// Получить клиента вместе с выданными ему промомкодами.
        /// </summary>
        /// <param name="id">Customer's id.</param>
        /// <response code="200">Sucsess.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdIncludeAsync(
                id,
                c => c.PromoCodes,
                c => c.Preferences);

            var response = customer.ToCustomerResponse();

            return Ok(response);
        }

        /// <summary>
        /// Создать клиента вместе с предпочтениями.
        /// </summary>
        /// <param name="request">Data for creating a new customer.</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = await GetCustomerAsync(request);

            await _customerRepository.AddAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Изменить клиента и дополнить список его предпочтений.
        /// </summary>
        /// <param name="id">Customer's id.</param>
        /// <param name="request">Data for edit customer.</param>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await GetCustomerAsync(request);

            customer.Id = id;

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента из базы данных всместе с промокодами.
        /// </summary>
        /// <param name="id">Customer's id.</param>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteAsync(id);

            return NoContent();
        }

        private async Task<Customer> GetCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdAsync(request.PreferenceIds);

            return request.ToCustomerWith(preferences);
        }
    }
}