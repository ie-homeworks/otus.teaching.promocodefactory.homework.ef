﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<CustomerPreference> _cpRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<CustomerPreference> cpRepository)
        {
            _cpRepository = cpRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить все промокоды.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(PromoCodeShortResponse[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();

            var response = promocodes.Select(p => p.ToPromoCodeShortResponse());

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customerIdPreferenceIdPairs = await _cpRepository.GetAllAsync();

            var promoCode = request.ToPromoCode();
            promoCode.CustomerPromoCodes = customerIdPreferenceIdPairs
                .Where(cp => cp.PreferenceId == request.PreferenceId)
                .Select(cp => new CustomerPromoCode { CustomerId = cp.CustomerId })
                .ToList();

            await _promoCodeRepository.AddAsync(promoCode);

            return NoContent();
        }
    }
}