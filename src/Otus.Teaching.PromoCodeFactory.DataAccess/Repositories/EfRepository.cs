﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected DataContext _context;

        public EfRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public Task<IEnumerable<T>> GetRangeByIdAsync(IEnumerable<Guid> ids)
        {
            return Task.FromResult(ids.Select(id => _context.Set<T>().FindAsync(id).Result));
        }

        public async Task<IEnumerable<T>> GetAllIncludeAsync(params Expression<Func<T, object>>[] includingEntities)
        {
            var query = _context.Set<T>().AsNoTracking();

            foreach (var e in includingEntities)
            {
                query = query.Include(e);
            }

            return await query.ToListAsync();
        }

        public async Task<T> GetByIdIncludeAsync(Guid id, params Expression<Func<T, object>>[] includingEntities) 
        {
            var query = _context.Set<T>().AsNoTracking();

            foreach (var e in includingEntities)
            {
                query = query.Include(e);
            }

            return await query.SingleAsync(x => x.Id == id);
        }

        public async Task<T> GetByIdAsync(Guid id, string includeEntityNames)
        {
            return await _context.Set<T>().Include(includeEntityNames).FirstAsync(x => x.Id == id);
        }

        public async Task AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);

            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var updated = _context.Set<T>().Update(entity);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);

            var removed = _context.Set<T>().Remove(entity);

            await _context.SaveChangesAsync();
        }
    }
}