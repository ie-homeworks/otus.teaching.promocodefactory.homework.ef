﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext([NotNull] DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            
            builder.Entity<Role>().HasData(FakeDataFactory.Roles);
            builder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            builder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);
            builder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomersPreferences);
            builder.Entity<CustomerPromoCode>().HasData(FakeDataFactory.CustomersPromoCodes);
        }
    }
}