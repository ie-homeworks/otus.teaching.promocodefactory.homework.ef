﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(c => c.FirstName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(c => c.LastName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(c => c.Email)
                .HasMaxLength(50)
                .IsRequired();

            builder.HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                j => j
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences),
                j => j
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences),
                j => j
                .HasKey(cp => new { cp.PreferenceId, cp.CustomerId }));

            builder.HasMany(c => c.PromoCodes)
                .WithMany(pc => pc.Customers)
                .UsingEntity<CustomerPromoCode>(
                j => j
                .HasOne(cpc => cpc.PromoCode)
                .WithMany(pc => pc.CustomerPromoCodes),
                j => j
                .HasOne(cpc => cpc.Customer)
                .WithMany(c => c.CustomerPromoCodes),
                j => j
                .HasKey(cpc => new { cpc.PromoCodeId, cpc.CustomerId }));
        }
    }
}