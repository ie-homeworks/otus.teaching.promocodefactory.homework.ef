﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(pc => pc.Code)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(pc => pc.ServiceInfo)
                .HasMaxLength(500);

            var dateTimeLength = DateTimeOffset.MaxValue.ToString().Length;

            builder.Property(pc => pc.BeginDate)
                .HasMaxLength(dateTimeLength)
                .IsFixedLength();

            builder.Property(pc => pc.EndDate)
                .HasMaxLength(dateTimeLength)
                .IsFixedLength();

            builder.Property(pc => pc.PartnerName)
                .HasMaxLength(50);

            builder.HasOne(pc => pc.PartnerManager)
                .WithMany();

            builder.HasOne(pc => pc.Preference)
                .WithMany();
        }
    }
}